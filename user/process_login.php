<?php 
/**................................................................
 * @package eblog v 1.0
 * @author Faith Awolu 
 * Hillsofts Technology Ltd.            
 * (hillsofts@gmail.com)
 * ................................................................
 */

include '../connect.php';
session_start();

function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}
	
	//Sanitize the POST values
	$login = $_POST['username'];
	$password = $_POST['password'];
	
	//Input Validations
	if($login == '') {
		$errmsg_arr[] = 'Username missing';
		$errflag = true;
	}
	if($password == '') {
		$errmsg_arr[] = 'Password missing';
		$errflag = true;
	}
	
	//If there are input validations, redirect back to the login form
	
	
	//Create query
	$qry="SELECT * FROM table_admin WHERE username='$login' AND password='$password'";
	$result=$db->prepare($qry);
	$result->execute();
	//Check whether the query was successful or not
	if($result) {
		if($result->rowCount() > 0) {
			//Login Successful
			session_regenerate_id();
			$member = $result->fetch();
			if(strcmp($member['role'], 'Admin') != 0){
					$_SESSION['SESS_USER_MEMBER_ID'] = $member['id'];
					$_SESSION['SESS_USER_FIRST_NAME'] = $member['name'];
					$_SESSION['SESS_USER_LAST_NAME'] = $member['email'];
					$_SESSION['SESS_USER_PRO_PIC'] = $member['file'];
					$_SESSION['SESS_USER_PRO_ROLE'] = $member['role'];
					
					unset($_SESSION['SESS_MEMBER_ID']);
					unset($_SESSION['SESS_FIRST_NAME']);
					unset($_SESSION['SESS_LAST_NAME']);
					unset($_SESSION['SESS_PRO_PIC']);
					unset($_SESSION['SESS_PRO_ROLE']);
					session_write_close();
					header("location: http://localhost/News-Blog/user/index.php");
			}else{
				$_SESSION['SESS_MEMBER_ID'] = $member['id'];
				$_SESSION['SESS_FIRST_NAME'] = $member['name'];
				$_SESSION['SESS_LAST_NAME'] = $member['email'];
				$_SESSION['SESS_PRO_PIC'] = $member['file'];
				$_SESSION['SESS_PRO_ROLE'] = $member['role'];
				
				unset($_SESSION['SESS_USER_MEMBER_ID']);
				unset($_SESSION['SESS_USER_FIRST_NAME']);
				unset($_SESSION['SESS_USER_LAST_NAME']);
				unset($_SESSION['SESS_USER_PRO_PIC']);
				unset($_SESSION['SESS_USER_PRO_ROLE']);
				session_write_close();
				header("location: http://localhost/News-Blog/admin/index.php");
			}
			exit();
		}else {
			
  echo '<script language = "javascript">';
  // echo "window.location.href='login.php'"; 
  echo "alert('Something went wrong, Enter correct details');window.location.href='login.php'";
   echo '</script>';
    exit;
   // echo "<script language = 'javascript'> alert('Wrong Details');'</script>";
                       
                       
                    }
	}else {
		die("Query failed");
	}
?>




