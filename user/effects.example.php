    <?php
    include 'image_editor.php';
    //Initialize the class
      $se=new SImEdi('test_image.jpg');
    //Load the effects plugin
        $se->plugin('effects');
    //Blur the image and save
       $se->effects->blur(10);
       $se->save('test_image.jpg');

    //load a different image ,emboss then save
       $se->load('php.png');
       $se->effects->emboss();
        $se->save('php2.png');