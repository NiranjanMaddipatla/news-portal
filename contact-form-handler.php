<?php 
/**................................................................
 * @package eblog v 1.0
 * @author Faith Awolu 
 * Hillsofts Technology Ltd.            
 * (hillsofts@gmail.com)
 * ................................................................
 */
include 'connect.php';
$settings = ORM::for_table("settings")
        ->find_one();
$errors = '';
$myemail = $settings->email;//<-----Put Your email address here.
if(empty($_POST['name'])  || 
   empty($_POST['email']))
{
    $errors .= "\n Error: all fields are required";
}
$name = $_POST['name']; 
$email_address = $_POST['email']; 
$message = "Please contact me"; 

if (!preg_match(
"/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i", 
$email_address))
{
    $errors .= "\n Error: Invalid email address";
}
if( empty($errors))
{
	$to = $myemail; 
	$email_subject = "Contact form submission: $name";
	$email_body = "You have received a new mail from $settings->site_name;. ".
	" Here are the details:\n Name: $name \n Email: $email_address \n Message \n $message"; 
	
	$headers = "From: $myemail\n"; 
	$headers .= "Reply-To: $email_address";
	
	mail($to,$email_subject,$email_body,$headers);        	
  echo '<script language = "javascript">';
  echo "alert('Thanks for contacting $settings->site_name. Will get back to you soon!');window.location.href='contact.php'";
   echo '</script>';
    exit;
} 
?>
