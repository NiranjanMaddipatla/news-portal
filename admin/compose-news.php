<?php include "header.php"; ?>
	<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="graphs">
					<h3 class="blank1">Post News</h3>
					<div class="xs">
						
						<div class="col-md-8 inbox_right">
							<div class="Compose-Message">               
								<div class="panel panel-default">
									<div class="panel-heading">
										Compose News 
									</div>
									 <?php if(get("success")):?>
                                            <div>
                                               <?=App::message("success", "News saved Successfully!")?>
                                            </div>
                                            <?php endif;?>
									<div class="panel-body panel-body-com-m">
										
										<form class="com-mail" action="save-news.php" method="post" enctype="multipart/form-data">
											
												<label>News Title : </label>
												<input type="text" name="news_title" class="form-control1 control1" placeholder="News Title" >
												<label>News Category : </label>
											<select id="news_category" name="news_category" class="form-control1 control1" onChange="getSubCat(this.value);">
											
											<option value="">Select Category </option>
											<?php $result = $db->prepare("SELECT * FROM category where status=1");
												$result->execute();
												for($i=1; $row = $result->fetch(); $i++){
											?>	
												  <option value="<?php echo $row['cat_name'];?>"><?php echo $row['cat_name'];?></option>
											<?php }?></select>
												<label>News Sub-Category : </label>
												<select id="news_subcategory" name="news_subcategory"class="form-control1 control1">
												 
												</select>
												
												<label>News Detail : </label>
												<textarea rows="6" id="body" name="news_detail" class="form-control1 control2"></textarea>
												 <script>
                CKEDITOR.replace( 'body' );
            </script>
			
							<label>Add Photo</label>
							<input type="file" name="file" class="form-control1 control3">
						
											<hr>
											<input type="submit" value="Submit News">
										</form>
									</div>
								 </div>
							  </div>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
		</div>
		<script>
function getSubCat(val) {
  $.ajax({
  type: "POST",
  url: "get_subcategory.php",
  data:'cat_name='+val,
  success: function(data){
    $("#news_subcategory").html(data);
  }
  });
  }
  </script>
		<?php include "footer.php"; ?>