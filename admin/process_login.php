<?php 
/**................................................................
 * @package eblog v 1.0
 * @author Faith Awolu 
 * Hillsofts Technology Ltd.            
 * (hillsofts@gmail.com)
 * ................................................................
 */

include '../connect.php';
session_start();

function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return mysql_real_escape_string($str);
	}
	
	//Sanitize the POST values
	$login = $_POST['username'];
	$password = $_POST['password'];
	$errflag = false;
	$login = str_replace('Username','',$login);
	$password = str_replace('password','',$password);
	if(empty(trim($login))) {
		$errmsg_arr = 'Username';
		$errflag = true;
	}
	if(empty(trim($password))) {
		$errmsg_arr = empty($errmsg_arr)?'Password':$errmsg_arr.',Password';
		$errflag = true;
	}
	
	if($errflag){
		echo '<script language = "javascript">';
		echo "alert(' ".$errmsg_arr." missing');window.location.href='sign-in.php'";
		echo '</script>';
		exit;
	}
	//If there are input validations, redirect back to the login form
	
	
	//Create query
	$qry="SELECT * FROM table_admin WHERE username='$login' AND password='$password'";
	$result=$db->prepare($qry);
	$result->execute();
	//Check whether the query was successful or not
	if($result) {
		if($result->rowCount() == 1) {
			//Login Successful
			session_regenerate_id();
			$member = $result->fetch();
			$_SESSION['SESS_MEMBER_ID'] = $member['id'];
			$_SESSION['SESS_FIRST_NAME'] = $member['name'];
			$_SESSION['SESS_LAST_NAME'] = $member['email'];
			$_SESSION['SESS_PRO_PIC'] = $member['file'];
			$_SESSION['SESS_PRO_ROLE'] = $member['role'];
			session_write_close();
			header("location: index.php");
			exit();
		}else if($result->rowCount() > 1) {
			echo '<script language = "javascript">';
			echo "alert('Something went wrong, More accounts with single email.');window.location.href='sign-in.php'";
			echo '</script>';
		}else{
			
			echo '<script language = "javascript">';
			echo "alert('Something went wrong, Enter correct details');window.location.href='sign-in.php'";
			echo '</script>';
			exit;                       
		}
	}else {
		die("Query failed");
	}
?>




