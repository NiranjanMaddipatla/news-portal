<?php include "header.php"; 
	$id=$_GET['id'];
	$result = $db->prepare("SELECT * FROM news WHERE id= :post_id");
	$result->bindParam(':post_id', $id);
	$result->execute();
	for($i=1; $row = $result->fetch(); $i++){
	?>
	
	<div id="page-wrapper">
				<div class="graphs">
					<h3 class="blank1">Edit News</h3>
					<div class="xs">
						
						<div class="col-md-8 inbox_right">
							<div class="Compose-Message">               
								<div class="panel panel-default">
									<div class="panel-heading">
										Compose News 
									</div>
									 <?php if(get("success")):?>
                                            <div>
                                               <?=App::message("success", "News edited Successfully!")?>
                                            </div>
                                            <?php endif;?>
									<div class="panel-body panel-body-com-m">
										
										<form class="com-mail" action="save-news.php" method="post" enctype="multipart/form-data">
											
												<label>News Title : </label>
												<input type="text" name="news_title" value="<?php echo $row['news_title'];?>" class="form-control1 control3" placeholder="News Title" >
												<label>News Category : </label>
											<select id="news_category" name="news_category" class="form-control1 control1" onChange="getSubCat(this.value);">
											
											<option value="">Select Category </option>
											<?php $result = $db->prepare("SELECT * FROM category where status=1");
												$result->execute();
												for($i=1; $rows = $result->fetch(); $i++){
											?>	
												  <option value="<?php echo $rows['cat_name'];?>" <?php echo ($row['cat_name']==$rows['cat_name'])? "selected": ""?>><?php echo $rows['cat_name'];?></option>
											<?php }?></select>
												<label>News Sub-Category : </label>
												<select id="news_subcategory" name="news_subcategory"class="form-control1 control1">
												 <option value="<?php echo htmlentities($row['sub_catname']);?>"><?php echo htmlentities($row['sub_catname']);?></option>
												</select>
												<label>News Detail : </label>
												<textarea rows="6" id="body" name="news_detail" class="form-control1 control2"><?php echo $row['news_detail'];?></textarea>
												 <script>
                CKEDITOR.replace( 'body' );
            </script>
			
							<label>Photo</label>
							<article class="post-content">
							<div class="featured-image">
								<img src="../uploads/<?php echo $row['file'];?>" id="cropbox" style="width:50%;height:50%;" >
							</div>
							<div id="btn">
								<input type='button' id="crop" value='CROP'>
							</div>
							<div>
								<img src="#" id="cropped_img" style="display: none;">
							</div>
								</article>
									<hr>
											<input type="submit" value="Submit News">
										</form>
									</div>
								 </div>
							  </div>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
		</div>
	<?php }include "footer.php"; ?>
	
	<script type="text/javascript">
$(document).ready(function(){
    var size;
    $('#cropbox').Jcrop({
      aspectRatio: 1,
      onSelect: function(c){
       size = {x:c.x,y:c.y,w:c.w,h:c.h};
       $("#crop").css("visibility", "visible");     
      }
    });
 
    $("#crop").click(function(){
        var img = $("#cropbox").attr('src');
        $("#cropped_img").show();
        $("#cropped_img").attr('src','image-crop.php?x='+size.x+'&y='+size.y+'&w='+size.w+'&h='+size.h+'&img='+img);
    });
});
</script>
	
	<script>
function getSubCat(val) {
  $.ajax({
  type: "POST",
  url: "get_subcategory.php",
  data:'cat_name='+val,
  success: function(data){
    $("#news_subcategory").html(data);
  }
  });
  }
  </script>